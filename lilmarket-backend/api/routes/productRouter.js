import express from "express";
import productController from "../controllers/productController.js";

const router = express.Router();
/*
 * GET
 */
router.get("/", productController.list);

/*
 * GET
 */
router.get("/:id", productController.read);

/*
 * POST
 */
router.post("/", productController.create);

/*
 * PUT
 */
router.put("/:id", productController.update);

/*
 * DELETE
 */
router.delete("/:id", productController.delete);

module.exports = router;
