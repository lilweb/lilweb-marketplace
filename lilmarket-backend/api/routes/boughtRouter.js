import express from "express";
import boughtController from "../controllers/boughtController.js";

const router = express.Router();
/*
 * GET
 */
router.get("/", boughtController.list);

/*
 * GET
 */
router.get("/:id", boughtController.read);

/*
 * POST
 */
router.post("/", boughtController.create);

/*
 * PUT
 */
router.put("/:id", boughtController.update);

/*
 * DELETE
 */
router.delete("/:id", boughtController.delete);

module.exports = router;
