import express from "express";
import typeController from "../controllers/typeController.js";

const router = express.Router();
/*
 * GET
 */
router.get("/", typeController.list);

/*
 * GET
 */
router.get("/:id", typeController.read);

/*
 * POST
 */
router.post("/", typeController.create);

/*
 * PUT
 */
router.put("/:id", typeController.update);

/*
 * DELETE
 */
router.delete("/:id", typeController.delete);

module.exports = router;
