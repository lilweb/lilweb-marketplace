import express from "express";
import tariffController from "../controllers/tariffController.js";

const router = express.Router();
/*
 * GET
 */
router.get("/", tariffController.list);

/*
 * GET
 */
router.get("/:id", tariffController.read);

/*
 * POST
 */
router.post("/", tariffController.create);

/*
 * PUT
 */
router.put("/:id", tariffController.update);

/*
 * DELETE
 */
router.delete("/:id", tariffController.delete);

module.exports = router;
