import express from "express";
import leadController from "../controllers/leadController.js";

const router = express.Router();
/*
 * GET
 */
router.get("/", leadController.list);

/*
 * GET
 */
router.get("/:id", leadController.read);

/*
 * POST
 */
router.post("/", leadController.create);

/*
 * PUT
 */
router.put("/:id", leadController.update);

/*
 * DELETE
 */
router.delete("/:id", leadController.delete);

module.exports = router;
