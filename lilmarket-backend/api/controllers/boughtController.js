import boughtModel from "../models/bought";

const isObjectId = (id) => {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = {
  read: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      boughtModel.findOne({ _id: req.params.id }).then(function(data) {
        if (!data) {
          return res.send("cannot find bought");
        } else {
          return res.json({ status: "success", data: data });
        }
      });
    }
    return res.json({ status: "error", msg: "No boughts found" });
  },

  list: (req, res) => {
    boughtModel.find({}).then(function(data) {
      if (!data) {
        return res.json({ status: "error", msg: "No boughts found" });
      } else {
        return res.json({ status: "success", data: data });
      }
    });
  },

  create: (req, res) => {
    var bought = Object.assign({}, req.body);
    boughtModel.create(bought, (err, data) => {
      if (err) {
        return res.status(400).json({ status: "error", err: err });
      }
      return res.status(201).json({ status: "success", data: data });
    });
  },

  update: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      boughtModel.findOne({ _id: id }).then(function(bought) {
        if (!bought) {
          return res.send("cannot find bought");
        } else {
          bought = Object.assign(data, req.body);
          bought.save((err, data) => {
            if (err) {
              return res.status(400).json({ status: "error", err: err });
            }
            return res.status(201).json({ status: "success", data: data });
          });
        }
      });
    }
    return res.json({ status: "error", msg: "No boughts found" });
  },

  delete: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      boughtModel.deleteOne(
        { _id: id},
        err => {
          if (err) {
            return res.status(400).json({ status: "error", err: err });
          }
          return res.status(201).json({ status: "success" });
        }
      );
    }
    return res.json({ status: "error", msg: "No boughts found" });
  }
};
