import userModel from "../models/user";

const isObjectId = (id) => {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = {
  read: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      userModel.findOne({ _id: req.params.id }).then(function(data) {
        if (!data) {
          return res.send("cannot find user");
        } else {
          return res.json({ status: "success", data: data });
        }
      });
    }
    return res.json({ status: "error", msg: "No users found" });
  },

  list: (req, res) => {
    userModel.find({}).then(function(data) {
      if (!data) {
        return res.json({ status: "error", msg: "No users found" });
      } else {
        return res.json({ status: "success", data: data });
      }
    });
  },

  create: (req, res) => {
    var user = Object.assign({}, req.body);
    userModel.create(user, (err, data) => {
      if (err) {
        return res.status(400).json({ status: "error", err: err });
      }
      return res.status(201).json({ status: "success", data: data });
    });
  },

  update: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      userModel.findOne({ _id: id }).then(function(user) {
        if (!user) {
          return res.send("cannot find user");
        } else {
          user = Object.assign(data, req.body);
          user.save((err, data) => {
            if (err) {
              return res.status(400).json({ status: "error", err: err });
            }
            return res.status(201).json({ status: "success", data: data });
          });
        }
      });
    }
    return res.json({ status: "error", msg: "No users found" });
  },

  delete: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      userModel.deleteOne(
        { _id: id},
        err => {
          if (err) {
            return res.status(400).json({ status: "error", err: err });
          }
          return res.status(201).json({ status: "success" });
        }
      );
    }
    return res.json({ status: "error", msg: "No users found" });
  }
};
