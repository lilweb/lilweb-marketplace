import leadModel from "../models/lead";

const isObjectId = (id) => {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = {
  read: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      leadModel.findOne({ _id: req.params.id }).then(function(data) {
        if (!data) {
          return res.send("cannot find lead");
        } else {
          return res.json({ status: "success", data: data });
        }
      });
    }
    return res.json({ status: "error", msg: "No leads found" });
  },

  list: (req, res) => {
    leadModel.find({}).then(function(data) {
      if (!data) {
        return res.json({ status: "error", msg: "No leads found" });
      } else {
        return res.json({ status: "success", data: data });
      }
    });
  },

  create: (req, res) => {
    var lead = Object.assign({}, req.body);
    leadModel.create(lead, (err, data) => {
      if (err) {
        return res.status(400).json({ status: "error", err: err });
      }
      return res.status(201).json({ status: "success", data: data });
    });
  },

  update: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      leadModel.findOne({ _id: id }).then(function(lead) {
        if (!lead) {
          return res.send("cannot find lead");
        } else {
          lead = Object.assign(data, req.body);
          lead.save((err, data) => {
            if (err) {
              return res.status(400).json({ status: "error", err: err });
            }
            return res.status(201).json({ status: "success", data: data });
          });
        }
      });
    }
    return res.json({ status: "error", msg: "No leads found" });
  },

  delete: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      leadModel.deleteOne(
        { _id: id},
        err => {
          if (err) {
            return res.status(400).json({ status: "error", err: err });
          }
          return res.status(201).json({ status: "success" });
        }
      );
    }
    return res.json({ status: "error", msg: "No leads found" });
  }
};
