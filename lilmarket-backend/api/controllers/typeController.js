import typeModel from "../models/type";

const isObjectId = (id) => {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = {
  read: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      typeModel.findOne({ _id: req.params.id }).then(function(data) {
        if (!data) {
          return res.send("cannot find type");
        } else {
          return res.json({ status: "success", data: data });
        }
      });
    }
    return res.json({ status: "error", msg: "No types found" });
  },

  list: (req, res) => {
    typeModel.find({}).then(function(data) {
      if (!data) {
        return res.json({ status: "error", msg: "No types found" });
      } else {
        return res.json({ status: "success", data: data });
      }
    });
  },

  create: (req, res) => {
    var type = Object.assign({}, req.body);
    typeModel.create(type, (err, data) => {
      if (err) {
        return res.status(400).json({ status: "error", err: err });
      }
      return res.status(201).json({ status: "success", data: data });
    });
  },

  update: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      typeModel.findOne({ _id: id }).then(function(type) {
        if (!type) {
          return res.send("cannot find type");
        } else {
          type = Object.assign(data, req.body);
          type.save((err, data) => {
            if (err) {
              return res.status(400).json({ status: "error", err: err });
            }
            return res.status(201).json({ status: "success", data: data });
          });
        }
      });
    }
    return res.json({ status: "error", msg: "No types found" });
  },

  delete: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      typeModel.deleteOne(
        { _id: id},
        err => {
          if (err) {
            return res.status(400).json({ status: "error", err: err });
          }
          return res.status(201).json({ status: "success" });
        }
      );
    }
    return res.json({ status: "error", msg: "No types found" });
  }
};
