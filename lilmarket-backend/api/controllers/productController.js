import productModel from "../models/product";

const isObjectId = (id) => {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = {
  read: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      productModel.findOne({ _id: req.params.id }).then(function(data) {
        if (!data) {
          return res.send("cannot find product");
        } else {
          return res.json({ status: "success", data: data });
        }
      });
    }
    return res.json({ status: "error", msg: "No products found" });
  },

  list: (req, res) => {
    productModel.find({}).then(function(data) {
      if (!data) {
        return res.json({ status: "error", msg: "No products found" });
      } else {
        return res.json({ status: "success", data: data });
      }
    });
  },

  create: (req, res) => {
    var product = Object.assign({}, req.body);
    productModel.create(product, (err, data) => {
      if (err) {
        return res.status(400).json({ status: "error", err: err });
      }
      return res.status(201).json({ status: "success", data: data });
    });
  },

  update: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      productModel.findOne({ _id: id }).then(function(product) {
        if (!product) {
          return res.send("cannot find product");
        } else {
          product = Object.assign(data, req.body);
          product.save((err, data) => {
            if (err) {
              return res.status(400).json({ status: "error", err: err });
            }
            return res.status(201).json({ status: "success", data: data });
          });
        }
      });
    }
    return res.json({ status: "error", msg: "No products found" });
  },

  delete: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      productModel.deleteOne(
        { _id: id},
        err => {
          if (err) {
            return res.status(400).json({ status: "error", err: err });
          }
          return res.status(201).json({ status: "success" });
        }
      );
    }
    return res.json({ status: "error", msg: "No products found" });
  }
};
