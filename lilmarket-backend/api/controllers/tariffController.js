import tariffModel from "../models/tariff";

const isObjectId = (id) => {
  return id.match(/^[0-9a-fA-F]{24}$/);
}

module.exports = {
  read: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      tariffModel.findOne({ _id: req.params.id }).then(function(data) {
        if (!data) {
          return res.send("cannot find tariff");
        } else {
          return res.json({ status: "success", data: data });
        }
      });
    }
    return res.json({ status: "error", msg: "No tariffs found" });
  },

  list: (req, res) => {
    tariffModel.find({}).then(function(data) {
      if (!data) {
        return res.json({ status: "error", msg: "No tariffs found" });
      } else {
        return res.json({ status: "success", data: data });
      }
    });
  },

  create: (req, res) => {
    var tariff = Object.assign({}, req.body);
    tariffModel.create(tariff, (err, data) => {
      if (err) {
        return res.status(400).json({ status: "error", err: err });
      }
      return res.status(201).json({ status: "success", data: data });
    });
  },

  update: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      tariffModel.findOne({ _id: id }).then(function(tariff) {
        if (!tariff) {
          return res.send("cannot find tariff");
        } else {
          tariff = Object.assign(data, req.body);
          tariff.save((err, data) => {
            if (err) {
              return res.status(400).json({ status: "error", err: err });
            }
            return res.status(201).json({ status: "success", data: data });
          });
        }
      });
    }
    return res.json({ status: "error", msg: "No tariffs found" });
  },

  delete: (req, res) => {
    const id = req.params.id;
    if (isObjectId(id)){
      tariffModel.deleteOne(
        { _id: id},
        err => {
          if (err) {
            return res.status(400).json({ status: "error", err: err });
          }
          return res.status(201).json({ status: "success" });
        }
      );
    }
    return res.json({ status: "error", msg: "No tariffs found" });
  }
};
