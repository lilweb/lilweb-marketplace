import mongoose from "mongoose";

const Schema = mongoose.Schema;
const TariffSchema = new Schema({
  tariff: ["requestCount", "requestSize", "worktime", "fixedSubscription", "oneTimePurchase", "payAsYoGo"],
  requestCount: {
    cost: Number,
    value: Number,
    maxValue: Number,
  },
  requestSize: {
    cost: Number,
    value: Number,
    maxValue: Number,
  },
  worktime: {
    cost: Number,
    value: Number,
    maxValue: Number,
  },
  fixedSubscription: {
    cost: Number,
    subscribeDate: Date,
    expiryDate: Date,
  },
  payAsYoGo: {
    cost: Number,
    constiant: ["requestCount", "requestSize", "worktime"],
  }
});

module.exports = mongoose.model("Tariff", TariffSchema);
