import mongoose from "mongoose";

const Schema = mongoose.Schema;
const BoughtSchema = new Schema({
    tariff: { type: Schema.Types.ObjectId, ref: "User" },
    type: { type: Schema.Types.ObjectId, ref: "Type" },
    version: String,
    date: Date,
});

module.exports = mongoose.model("Bought", BoughtSchema);
