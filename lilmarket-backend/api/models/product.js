import mongoose from "mongoose";
const Schema = mongoose.Schema;
const ProductSchema = new Schema({
  owner: { type: Schema.Types.ObjectId, ref: "User" },
  name: String,
  image: String,
  slug: {
    type: String,
    unique: true,
    required: true,
  },
  version: {
    type: String,
    unique: true,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  tariffs: [{ type: Schema.Types.ObjectId, ref: "Tariff" }],
  type: { type: Schema.Types.ObjectId, ref: "Type" },
  integrationDeps: [{ type: Schema.Types.ObjectId, ref: "Product" }],
});

module.exports = mongoose.model("Product", ProductSchema);
