import mongoose from "mongoose";

const Schema = mongoose.Schema;
const TypeSchema = new Schema({
  type: ["api", "repo", "exec", "saas", "paas", "iaas"],
  repo: {
    link: String,
    repoSshKey: String
  },
  api: {
    endpoint: String,
    apiTokens: [String],
  },
  iaas: {
    endpoint: String,
  },
  paas: {
    endpoint: String,
  },
  saas: {
    endpoint: String,
  },
  executable: {
    link: String,
    productKeyEndpoint: String,
  },
});

module.exports = mongoose.model("Type", TypeSchema);
