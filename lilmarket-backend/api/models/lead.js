import mongoose from "mongoose";

const Schema = mongoose.Schema;
const LeadSchema = new Schema({
    fullName: String,
    description: String,
    contacts: [ String ],
    status: String,
});

module.exports = mongoose.model("Lead", LeadSchema);
