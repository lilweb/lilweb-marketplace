import mongoose from "mongoose";
import bcrypt from "bcryptjs";
import { SALT_ROUNDS } from "../../config";
const Schema = mongoose.Schema;
const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  name: String,
  password: {
    type: String,
    required: true
  },
  userType: ["admin", "user", "saler", "integrator", "salesman", "support"],
  inn: String,
  leads: [{ type: Schema.Types.ObjectId, ref: "Lead" }],
  userStatus: ["active", "banned", "unverified"],
  boughtProducts: [{ type: Schema.Types.ObjectId, ref: "Product" }],
});

UserSchema.methods = {
  checkPassword: function(inputPassword) {
    return bcrypt.compareSync(inputPassword, this.password);
  },
  hashPassword: plainTextPassword => {
    return bcrypt.hashSync(plainTextPassword, SALT_ROUNDS);
  }
};

module.exports = mongoose.model("User", UserSchema);
