import express from "express";
import user from "./routes/userRouter";
import lead from "./routes/leadRouter";
import bought from "./routes/boughtRouter";
import product from "./routes/productRouter";
import tariff from "./routes/tariffRouter";
import typeRouter from "./routes/typeRouter";

const router = express.Router();

/*WARNING Unsecure routes part without JWT auth */

router.use("/user", user);
router.use("/lead", lead);
router.use("/bought", bought);
router.use("/product", product);
router.use("/tariff", tariff);
router.use("/typeRouter", typeRouter);

/* JWT auth */

// router.use((req, res, next) => {
//   //TODO generate userToken
// });

/* Secure routes */

module.exports = router;
