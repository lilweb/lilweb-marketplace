const express = require("express");
const app = express();
import https from "https";
import http from "http";
const helmet = require("helmet");
const bodyParser = require("body-parser");
import pkg from "./package";
import mongoose from "mongoose";
import { SSL_CERT, PORT, PROTO, hostname, DB_NAME } from "./config";
import api from "./api";

mongoose.connect(`mongodb://localhost:27017/${DB_NAME}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
app.use(helmet());
app.use(bodyParser.json());
if (PROTO === "HTTP") {
  http.createServer(app).listen(PORT, () => {
    console.log(
      `Server ${pkg.name} v${
        pkg.version
      } \nListening on:http://${hostname}:${PORT}`
    );
  });
} else {
  https
    .createServer(
      {
        key: SSL_CERT.key,
        cert: SSL_CERT.cert
      },
      app
    )
    .listen(PORT, () => {
      console.log(
        `Server ${pkg.name} v${
          pkg.version
        } \nListening on:https://${hostname}:${PORT}`
      );
    });
}
//api use
app.use("/api", api);

app.use("*", function(req, res) {
  return res.json({ status: "error", message: "Forbidden" });
});
