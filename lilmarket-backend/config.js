import fs from "fs";
import os from "os";
const SALT_ROUNDS = 10;
const PROTO = process.env.PROTO || "HTTP";
const hostname = os.hostname();
const DB_NAME = "marketplace";
let SSL_CERT = {};
if (PROTO === "HTTPS") {
  SSL_CERT = {
    key: fs.readFileSync(
      "/etc/letsencrypt/live/" + hostname + "/privkey.pem",
      "utf8"
    ),
    cert: fs.readFileSync(
      "/etc/letsencrypt/live/" + hostname + "/fullchain.pem",
      "utf8"
    )
  };
}
const PORT = 8080;
export {
  SALT_ROUNDS,
  SSL_CERT,
  PORT,
  PROTO,
  hostname
};
