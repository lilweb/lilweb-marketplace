import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexWrap: "wrap",
  },
  textField: {
    width: 200,
  },
  formControl: {
    margin: 15,
    minWidth: 120,
  },
});


export default function ProductsEdit() {
  const classes = useStyles();

  const [name, setName] = React.useState("");
  const handleName = (event) => {
    setName(event.target.value);
  };

  const [description, setDescription] = React.useState("");
  const handleDescription = (event) => {
    setDescription(event.target.value);
  };

  const [version, setVersion] = React.useState("");
  const handleVersion = (event) => {
    setVersion(event.target.value);
  };

  const [type, setType] = React.useState("");
  const handleType = (event) => {
    setType(event.target.value);
  };
  const [tariffType, settariffType] = React.useState("");
  const handletariffType = (event) => {
    settariffType(event.target.value);
  };

  const [photo, setPhoto] = React.useState("");
  const handlePhoto = (event) => {
    setPhoto(event.target.value);
  };
  const [prices, setPrices] = React.useState([{
    price: 0,
    quantity: 0,
  }]);
  const handlePrices = (event) => {
    setPrices(event.target.value);
  };
  const sendProduct = () => ({
    description,
    photo,
    name,
    version,
    tariffType,
    type,
  });
    // fetch(`${api}api/product/`, {
    //   body: JSON.stringify({
    //     category,
    //     owner,
    //     description,
    //     photo,
    //     name,
    //     version,
    //     prises,
    //     tariffType,
    //     type,
    //   }),
    //   headers: {
    //     Accept: "application/json",
    //     Authorization: `Bearer ${token}`,
    //     "Content-Type": "application/json",
    //   },
    //   method: "POST",
    // })
    //   .catch((error) => {
    //     console.log(error);
    //     Alert.alert("Проверьте подключение к интернету");
    //   })
    //   .then((response) => response.json())
    //   .then((responseJSON) => {
    //
    //   });
  console.log(sendProduct());

  return (

    <div className={classes.container}>
      <div>

        <TextField
          label="Название"
          style={{ margin: 15 }}
          placeholder="Введите название продукта"
          fullWidth
          onChange={handleName}
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="URL картинки"
          style={{ margin: 15 }}
          onChange={handlePhoto}
          placeholder="Введите url к фотографии, которая будет на обложке продукта"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="Описание"
          style={{ margin: 15 }}
          onChange={handleDescription}
          placeholder="Введите описание продукта"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <TextField
          label="Версия"
          onChange={handleVersion}
          style={{ margin: 15 }}
          placeholder="Введите версию продукта"
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
        />
        <FormControl className={classes.formControl}>
          <InputLabel>Тип продукта</InputLabel>
          <Select
            onChange={handleType}
          >
            <MenuItem value="api">Предоставление API</MenuItem>
            <MenuItem value="repo">Предоставление репозитория (по SSH)</MenuItem>
            <MenuItem value="requestSize">Трафик использования запросов</MenuItem>
            <MenuItem value="exec">Исполняемый файл</MenuItem>
            <MenuItem value="saas">SAAS</MenuItem>
            <MenuItem value="paas">PAAS</MenuItem>

          </Select>
          <FormHelperText>Выберите, какой из тарифов вы предложите пользователям</FormHelperText>
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel>Тип монетизации</InputLabel>
          <Select
            onChange={handletariffType}
          >

            <MenuItem value="requestCount">
            Ограничение кол-ва запросов
            </MenuItem>
            <MenuItem value="oneTimePurchase">Единоразовая покупка</MenuItem>
            <MenuItem value="requestSize">Трафик использования запросов</MenuItem>
            <MenuItem value="worktime">Время обработки запросов</MenuItem>
            <MenuItem value="fixedSubscription">Подписка</MenuItem>
            <MenuItem value="payAsYouGo">Гибкий тариф</MenuItem>

          </Select>
          <FormHelperText>Выберите, какой из тарифов вы предложите пользователям</FormHelperText>

        </FormControl>

        {tariffType === "requestCount" ? (
          <div>
            {prices.concat([{ price: 0, quantity: 0 }]).map(({ price, quantity }, index) => (
              <>
                <div>{`Тариф ${index + 1}`}</div>
                <TextField
                  label="Количество"
                  style={{ margin: 15 }}
                  onChange={(e) => {
                    console.log(e.target.value);
                    const upd = prices;
                    upd[index].quantity = e.target.value;
                    console.log(upd);
                    setPrices(upd);
                    // if (e.target.value) {
                    //   if (prices[index]) {
                    //     const upd = prices;
                    //     upd[index].quantity = e.target.value;
                    //     setPrices(upd);
                    //   } else {
                    //     setPrices(prices.concat([{ price: 0, quantity: e.target.value }]));
                    //   }
                    // } else if (prices.length > 1 && prices[index].price === "") {
                    //   const upd = prices;
                    //   upd.slice(index, 1);
                    //   setPrices(upd);
                    // }
                  }}
                  placeholder="Введите количество"
                  fullWidth
                  value={quantity}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  label="Цена"
                  style={{ margin: 15 }}
                  onChange={(e) => {
                    const upd = prices;
                    upd[index].price = e.target.value;
                    setPrices(upd);
                    // if (e.target.value) {
                    //   if (prices[index]) {
                    //     const upd = prices;
                    //     upd[index].price = e.target.value;
                    //     setPrices(upd);
                    //   } else {
                    //     setPrices(prices.concat([{ quantity: 0, price: e.target.value }]));
                    //   }
                    // } else if (prices.length > 1 && prices[index].quantity === "") {
                    //   const upd = prices;
                    //   upd.slice(index, 1);
                    //   setPrices(upd);
                    // }
                  }}
                  placeholder="Введите цену"
                  fullWidth
                  margin="normal"
                  value={price}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </>
            ))}
          </div>
        ) : (
          <div>
            <TextField
              label="Количество"
              style={{ margin: 15 }}
              onChange={handlePrices.quantity}
              placeholder="Введите количество"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              label="Цена"
              style={{ margin: 15 }}
              onChange={handlePrices.price}
              placeholder="Введите цену"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
}
