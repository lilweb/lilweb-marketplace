import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "03CE7E",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};

const useStyles = makeStyles(styles);

export default function TableList() {
  const classes = useStyles();
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Счета</h4>
            <p className={classes.cardCategoryWhite}>
            Здесь будут показываться денежные поступление за использование ваших продуктов
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["Дата операции", "Описание платежа", "Комиссия (%)", "Итоговая сумма"]}
              tableData={[
                ["01.12.2019", "Поступление ежемесячных средств за использование продукта 'React Native Carousel-Wrapper' (размещен в SberCloud)", "3%", "+$36,738"],
                ["01.11.2019", "Поступление ежемесячных средств за использование продукта 'SberLearning' (размещен в SberCloud)", "3%", "+$23,789"],
                ["01.10.2019", "Поступление ежемесячных средств за использование продукта 'React Native Carousel-Wrapper'", "8%", "+$26,142"],
                ["01.09.2019", "Поступление ежемесячных средств за использование продукта 'SberLearning'", "8%", "+$17,735"],
                ["01.08.2019", "Поступление ежемесячных средств за использование продуктa 'React Native Carousel-Wrapper' ", "8%", "+$63,542"],
                ["01.07.2019", "Поступление ежемесячных средств за использование продуктa 'SberLearning'", "8%", "+$78,615"],
                ["01.12.2018", "Поступление ежемесячных средств за использование продукта 'React Native Carousel-Wrapper' (размещен в SberCloud)", "3%", "+$36,738"],
                ["01.11.2018", "Поступление ежемесячных средств за использование продукта 'SberLearning' (размещен в SberCloud)", "3%", "+$23,789"],
                ["01.10.2018", "Поступление ежемесячных средств за использование продукта 'React Native Carousel-Wrapper'", "8%", "+$26,142"],
                ["01.09.2018", "Поступление ежемесячных средств за использование продукта 'SberLearning'", "8%", "+$17,735"],
                ["01.08.2018", "Поступление ежемесячных средств за использование продуктa 'React Native Carousel-Wrapper' ", "8%", "+$63,542"],
                ["01.07.2018", "Поступление ежемесячных средств за использование продуктa 'SberLearning'", "8%", "+$78,615"],
                ["01.12.2017", "Поступление ежемесячных средств за использование продукта 'React Native Carousel-Wrapper' (размещен в SberCloud)", "3%", "+$36,738"],
                ["01.11.2017", "Поступление ежемесячных средств за использование продукта 'SberLearning' (размещен в SberCloud)", "3%", "+$23,789"],
                ["01.10.2017", "Поступление ежемесячных средств за использование продукта 'React Native Carousel-Wrapper'", "8%", "+$26,142"],
                ["01.09.2017", "Поступление ежемесячных средств за использование продукта 'SberLearning'", "8%", "+$17,735"],
                ["01.08.2017", "Поступление ежемесячных средств за использование продуктa 'React Native Carousel-Wrapper' ", "8%", "+$63,542"],
                ["01.07.2017", "Поступление ежемесячных средств за использование продуктa 'SberLearning'", "8%", "+$78,615"],
              ]}
            />
          </CardBody>
        </Card>
      </GridItem>

    </GridContainer>
  );
}
