/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Copyright 2019 LilWeb (https://lilweb.ru)
*

* Coded by LilWeb

=========================================================

* The above copyright notice and this permission notice
shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import React from "react";
import Apps from "@material-ui/icons/Apps";
import TrendingUp from "@material-ui/icons/TrendingUp";
import Settings from "@material-ui/icons/Settings";
import AttachMoney from "@material-ui/icons/AttachMoney";
import ExitToApp from "@material-ui/icons/ExitToApp";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
// core components/views for Admin layout
import CloudDownload from "@material-ui/icons/CloudDownload";
import DashboardPage from "./views/Dashboard/Dashboard.js";
import UserProfile from "./views/UserProfile/UserProfile.js";
import Leads from "./views/Leads/Leads.js";
import Products from "./components/Products";
import CloudData from "./components/CloudData";
import Trello from "./components/Trello";
import ProductsEdit from "./views/Products/ProductsEdit";
import TableList from "./views/TableList/TableList.js";
import SupportList from "./views/SupportList/SupportList.js";
// core components/./views for RTL layout

const user = [
  {
    path: "/products",
    name: "Продукты",
    icon: Apps,
    component: () => <Products salerAcc title="Купленные продукты" />,
    layout: "/account",
  },
  {
    path: "/invoices",
    name: "Счета",
    icon: AttachMoney,
    component: TableList,
    layout: "/account",
  },
  {
    path: "/integrations",
    name: "Получить интеграцию",
    icon: AttachMoney,
    component: () => (
      <div>
Здесь скоро можно будет заказать
       интеграцию у сертифицированных специалистов,
       которые помогут перенести ваш бизнес в облако,
       а также подключить комплексное решение из нескольких сервисов,
       связанных между собой
      </div>
    ),
    layout: "/account",
  },
  {
    path: "/settings",
    name: "Настройки",
    component: UserProfile,
    layout: "/account",
    icon: Settings,
  },
  {
    path: "/tohomepage",
    name: "На главную",
    component: UserProfile,
    layout: "/account",
    icon: ChevronLeft,
  },
  {
    path: "/logout",
    name: "Выйти",
    component: UserProfile,
    layout: "/account",
    icon: ExitToApp,
  },
];
const integrator = [
  {
    path: "/products",
    name: "Продукты",
    icon: Apps,
    component: () => <Products salerAcc title="Управление продуктами" />,
    layout: "/account",
  },
  {
    path: "/integrations",
    name: "Кросс-функциональные интеграции",
    icon: Apps,
    component: () => (
      <div>
Здесь скоро можно будет получать заказы
        на создание комплексных интеграций между
        несколькими из представленных на сайте сервисов.
      </div>
    ),
    layout: "/account",
  },
  {
    path: "/product-edit",
    name: "Добавление продукта",
    icon: Apps,
    component: ProductsEdit,
    layout: "/account",
  },
  {
    path: "/dashboard",
    name: "Аналитика и продажи",
    icon: TrendingUp,
    component: DashboardPage,
    layout: "/account",
  },
  {
    path: "/invoices",
    name: "Счета",
    icon: AttachMoney,
    component: TableList,
    layout: "/account",
  },
  {
    path: "/leads",
    name: "Биржа Лидов",
    icon: CloudDownload,
    component: Leads,
    layout: "/account",
  },
  {
    path: "/settings",
    name: "Настройки",
    component: UserProfile,
    layout: "/account",
    icon: Settings,
  },
  {
    path: "/tohomepage",
    name: "На главную",
    component: UserProfile,
    layout: "/account",
    icon: ChevronLeft,
  },
  {
    path: "/logout",
    name: "Выйти",
    component: UserProfile,
    layout: "/account",
    icon: ExitToApp,
  },
];
const salesman = [
  {
    path: "/sales-deck",
    name: "Воронка продаж",
    icon: CloudDownload,
    component: Trello,
    layout: "/account",
  },
];
const admin = [
  {
    path: "/products",
    name: "Продукты",
    icon: Apps,
    component: () => <Products salerAcc title="Управление продуктами" />,
    layout: "/account",
  },

  {
    path: "/userlist",
    name: "Список пользователей",
    icon: Apps,
    component: () => (
      <div>
Здесь скоро можно будет просмотреть
        список пользователей, а также блокировать,
        замораживать аккаунты..
      </div>
    ),
    layout: "/account",
  },

  {
    path: "/integrations",
    name: "Кросс-функциональные интеграции",
    icon: Apps,
    component: () => (
      <div>
Здесь скоро можно будет просмотреть
        комплексные интеграции между несколькими из
        представленных на сайте сервисов.
      </div>
    ),
    layout: "/account",
  },

  {
    path: "/product-edit",
    name: "Добавление продукта",
    icon: Apps,
    component: ProductsEdit,
    layout: "/account",
  },
  {
    path: "/dashboard",
    name: "Общая аналитика по сайту",
    icon: TrendingUp,
    component: DashboardPage,
    layout: "/account",
  },
  {
    path: "/invoices",
    name: "Счета",
    icon: AttachMoney,
    component: TableList,
    layout: "/account",
  },
  {
    path: "/data",
    name: "Данные в облаке",
    icon: CloudDownload,
    component: TableList,
    layout: "/account",
  },
  {
    path: "/settings",
    name: "Настройки",
    component: UserProfile,
    layout: "/account",
    icon: Settings,
  },
  {
    path: "/tohomepage",
    name: "На главную",
    component: UserProfile,
    layout: "/account",
    icon: ChevronLeft,
  },
  {
    path: "/logout",
    name: "Выйти",
    component: UserProfile,
    layout: "/account",
    icon: ExitToApp,
  },
];
const support = [
  {
    path: "/support-list",
    name: "Заявки на сетрификацию",
    icon: CloudDownload,
    component: SupportList,
    layout: "/account",
  },
];
const saler = [
  {
    path: "/products",
    name: "Продукты",
    icon: Apps,
    component: () => <Products cardIcons salerAcc title="Управление продуктами" />,
    layout: "/account",
  },
  {
    path: "/product-edit",
    name: "Добавление продукта",
    icon: Apps,
    component: ProductsEdit,
    layout: "/account",
  },
  {
    path: "/dashboard",
    name: "Аналитика и продажи",
    icon: TrendingUp,
    component: DashboardPage,
    layout: "/account",
  },
  {
    path: "/invoices",
    name: "Счета",
    icon: AttachMoney,
    component: TableList,
    layout: "/account",
  },
  {
    path: "/data",
    name: "Данные в облаке",
    icon: CloudDownload,
    component: CloudData,
    layout: "/account",
  },
  {
    path: "/settings",
    name: "Настройки",
    component: UserProfile,
    layout: "/account",
    icon: Settings,
  },
  {
    path: "/tohomepage",
    name: "На главную",
    component: UserProfile,
    layout: "/account",
    icon: ChevronLeft,
  },
  {
    path: "/logout",
    name: "Выйти",
    component: UserProfile,
    layout: "/account",
    icon: ExitToApp,
  },
];
const getRoutes = (accountType) => {
  console.log(accountType);
  switch (accountType) {
    case "saler":
      return saler;
    case "user":
      return user;
    case "support":
      return support;
    case "integrator":
      return integrator;
    case "salesman":
      return salesman;
    case "admin":
      return admin;
    default:
  }
  return [];
};

export default getRoutes;
