import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ReactMarkdown from "react-markdown";
import {
  Redirect,
} from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Header } from "../components";
import filee from "../mdtemplate.md";

const productInfo = {
  naming: "React Native Snap-Carousel.",
  description: "Swiper component for React Native featuring previews, multiple layouts, parallax images, performant handling of huge numbers of items, and RTL support",
  pricing: 9999,
  image: "https://reactnativeexample.com/content/images/2017/08/20170809103344.jpg",
  price1: "99$",
  tariff1: "10 000 запросов",
  price2: "149$",
  tariff2: "20 000 запросов",
  price3: "299$",
  tariff3: "75 000 запросов",
};


const useStyles = (image) => makeStyles({

  titleWrapper: {
    textAlign: "center",
    margin: "50px 0",
    padding: "0px 20px",
  },
  title: {
    fontSize: "40px",
    fontWeight: 400,
    textAlign: "left",
  },
  productWrapper: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr))",
    margin: "30px auto",
    gap: "30px",
    maxWidth: "960px",
    padding: "0px 20px",
  },
  container: {
    height: "100%",

  },
  mainHeader: {
    width: "100%",
    minHeight: "100vh",
    display: "grid",
    gridTemplateColumns: "1fr",
    gridTemplateRows: "auto",
    zIndex: 2,
    background: "#ffffff",
  },
  link: {
    textDecoration: "none",
    color: "inherit",
  },
  bottomContainer: {
    position: "absolute",
    bottom: 0,
    left: "0px",
    width: "100%",
  },
  waveBlock: {
    alignSelf: "end",
    display: "flex",
  },
  backgroundImageBlock: {
    filter: "brightness(0.5)",
    height: "100vh",
    width: "100%",
    position: "absolute",
    top: "0px",
    left: "0px",
    background: `url(${image}) center bottom / cover`,
    backgroundPosition: "center center",
  },
  navBar: {
    dispay: "flex",
  },
  eHSAxJ: {
    "&:hover": {
      "& $logoBeatles": {
        opacity: 1,
      },
    },
    position: "relative",
    display: "flex",
    minHeight: "auto",
    transform: "translate3d(0px, 0px, 0px)",
    cursor: "pointer",
    boxSizing: "border-box",
    overflow: "hidden",
    padding: "40px",
    background: "transparent",
    borderRadius: "14px",
    borderWidth: "initial",
    borderStyle: "none",
    borderColor: "initial",
    borderImage: "initial",
    height: "50vh",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  cardTitle: {
    color: "#ffffff",
    fontSize: "60px",
    margin: 0,
    flex: 1,
    textAlign: "left",
  },
  cardTitleText: {
    color: "#ffffff",
    fontSize: "60px",
    margin: 0,
    textAlign: "left",
  },
  logoBeatles: {
    opacity: 0,
    position: "absolute",
    width: "100%",
    height: "100%",
    transition: "all 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    transform: "scale(1.6)",
    top: "-50%",
    left: "50%",
  },
  titleCard: {
    position: "absolute",
    top: "25vh",
    transform: "translate(50%)",
    height: "50vh",
    width: "50vw",
    boxShadow: "rgba(255, 255, 255, 0.5) 0px 10px 30px",
    transition: "box-shadow 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s, transform 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    overflow: "hidden",
    borderRadius: "18px",
    "&:hover": {
      boxShadow: "rgba(255, 255, 255, 0) 0px 10px 20px",


      transform: "translate(50%, -20px)",
    },
  },
  buttonGreen: {
    padding: "0.7em 1em",
    display: "flex",
    fontWeight: 600,
    fontSize: "20px",
    color: "white",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#03CE7E",
    transition: "box-shadow 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    borderRadius: "13px",
    "&:hover": {
      boxShadow: "rgba(3, 206, 126, 0.5) 0px 5px 15px",
    },
  },
  descrip: {
    height: "100%",
    maxWidth: "1012px",
    background: "#fff",
    borderRadius: "30px",
    padding: "40px 100px",
    margin: "0 auto",
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 5px 15px",
  },
  wrapper: {
    padding: "40px 0",
  },
  price: {
    justifyContent: "center",
    alignItems: "center",
    maxWidth: "1012px",
    flexDirection: "row",
    display: "flex",
    height: "20vh",
    margin: "0 auto",
  },
  priceBlock: {
    flex: 1,
    background: "#fff",
    padding: "10px",
    margin: " 20px",
    borderRadius: "14px",
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 10px 20px",
  },
})();

export default function Product() {
  const [input, setInput] = React.useState("");

  const readTextFile = (file) => {
    const rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = () => {
      if (rawFile.readyState === 4) {
        if (rawFile.status === 200 || rawFile.status === 0) {
          const allText = rawFile.responseText;
          setInput(allText);
        }
      }
    };
    rawFile.send(null);
  };
  React.useEffect(() => {
    readTextFile(filee);
  }, []);
  const classes = useStyles(productInfo.image);
  const [open, setOpen] = React.useState(false);
  const [type, setType] = React.useState("");
  const [loggedIn, setLoggedIn] = React.useState(null);

  const handleClose = () => {
    setOpen(false);
  };
  const checkAcc = () => {
    const accounts = ["admin", "user", "integrator", "saler", "salesman", "support"];
    if (accounts.includes(type)) {
      setLoggedIn(true);
    }
    setOpen(false);
  };
  return (
    <div className={classes.container}>
      <Header
        onClick={() => setOpen(true)}
        image={productInfo.image}
        title={productInfo.naming}
        buttonText="Купить"
      />
      <div className={classes.price}>
        <div className={classes.priceBlock}>
          <h3>{productInfo.price1}</h3>
          <h3>{productInfo.tariff1}</h3>
        </div>
        <div className={classes.priceBlock}>
          <h3>{productInfo.price2}</h3>
          <h3>{productInfo.tariff2}</h3>
        </div>
        <div className={classes.priceBlock}>
          <h3>{productInfo.price3}</h3>
          <h3>{productInfo.tariff3}</h3>
        </div>
      </div>
      <div className={classes.wrapper}>

        <div className={classes.descrip}>
          <div className={classes.titleWrapper}>
            <h3 className={classes.title}>{productInfo.description}</h3>
          </div>
          <div>
            <div>
              <ReactMarkdown source={input} />
            </div>
          </div>
        </div>
      </div>

      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Личный кабинет</DialogTitle>
        <DialogContent>
          <DialogContentText>
                  Введите роль пользователя:
            <br />
                  admin | user | integrator | saler | salesman | support
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Роль"
            placeholder="Роль"
            type="text"
            value={type}
            onChange={(e) => { setType(e.target.value); }}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
                  Отмена
          </Button>
          <Button onClick={checkAcc} color="primary">
                  Войти
          </Button>
        </DialogActions>
      </Dialog>
      {loggedIn ? <Redirect to={`/goToAccount/${type}`} /> : null}
    </div>
  );
}
