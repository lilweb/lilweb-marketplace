import React from "react";
import { makeStyles } from "@material-ui/core/styles";
// import {  }

const useStyles = makeStyles({
  container: {
    minHeight: "100vh",
    background: "white",
  },
});
export default function Account() {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      Account
    </div>
  );
}
