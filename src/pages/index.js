import Home from "./Home";
import Catalog from "./Catalog";
import Account from "./Account";
import Product from "./Product";

export {
  Catalog,
  Account,
  Product,
  Home,
};
