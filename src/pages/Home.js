import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { Header, Products } from "../components";

const useStyles = makeStyles({
  container: {
    minHeight: "100vh",
    background: "#eeeeee",
  },
});
function Home(props) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Header
        main
        onClick={() => props.history.push("/catalog")}
        image="https://elevateyourexperiences.com/wp-content/uploads/2018/02/elevate-website-background.jpg"
        title={(
          <>
Маркетплейс
            <br />
            {" "}
IT-решений
          </>
)}
        buttonText="В каталог"
      />
      <Products title="Популярные сервисы" />
    </div>
  );
}
export default withRouter(Home);
