import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import { Header, Products } from "../components";

const useStyles = makeStyles({
  container: {
    minHeight: "100vh",
  },
  productCardWrapper: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr))",
    margin: "30px auto",
    gap: "20px",
    maxWidth: "960px",
    padding: "0px 20px",
  },
});
function Catalog(props) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Header
        onClick={() => props.history.push("/account")}
        main
        image="https://i.pinimg.com/originals/17/01/af/1701af5764921ba974a04776c0c2859b.jpg"
        title="Каталог"
        buttonText="Мои покупки"
      />
      <Products title="API сервисы" />
      <Products title="Приложения" />
      <Products title="SaaS решения" />
      <Products title="PaaS решения" />
      <Products title="IaaS решения" />
    </div>
  );
}
export default withRouter(Catalog);
