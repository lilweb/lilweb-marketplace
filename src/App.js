import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  withRouter,
} from "react-router-dom";
import { createBrowserHistory } from "history";
// import logo from "./logo.svg";
import "./App.css";
import Admin from "./layouts/Admin.js";
import {
  Home, Catalog, Product,
} from "./pages";
import { NavBar, Footer } from "./components";
import "./assets/css/material-dashboard-react.css";


function ScrollToTopF(props) {
  React.useEffect(() => {
    window.scrollTo(0, 0);
  }, [props.location]);
  return props.children;
}
const ScrollToTop = withRouter(ScrollToTopF);

const withNavBarAndFooter = (Component) => (
  <>
    <NavBar />
    <Component />
    <Footer />
  </>
);


const CatalogPage = () => withNavBarAndFooter(Catalog);
const HomePage = () => withNavBarAndFooter(Home);
const ProductPage = () => withNavBarAndFooter(Product);
const hist = createBrowserHistory();
function App() {
  return (
    <Router history={hist}>
      <div className="App">
        <ScrollToTop>
          <Switch>
            <Redirect
              from="/goToAccount/admin"
              to={{
                pathname: "/account/",
                state: { accountType: "admin" },
              }}
            />
            <Redirect
              from="/goToAccount/user"
              to={{
                pathname: "/account/",
                state: { accountType: "user" },
              }}
            />
            <Redirect
              from="/goToAccount/support"
              to={{
                pathname: "/account/",
                state: { accountType: "support" },
              }}
            />
            <Redirect
              from="/goToAccount/saler"
              to={{
                pathname: "/account/",
                state: { accountType: "saler" },
              }}
            />
            <Redirect
              from="/goToAccount/salesman"
              to={{
                pathname: "/account/",
                state: { accountType: "salesman" },
              }}
            />
            <Redirect
              from="/goToAccount/integrator"
              to={{
                pathname: "/account/",
                state: { accountType: "integrator" },
              }}
            />
            <Redirect from="/account/tohomepage" to="/" />
            <Redirect from="/account/logout" to="/" />
            <Route path="/catalog">
              <CatalogPage />
            </Route>
            <Route path="/account" component={Admin} />
            {/* <Route path="/account">
              <Account />
            </Route> */}
            <Route path="/product">
              <ProductPage />
            </Route>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </ScrollToTop>
      </div>
    </Router>
  );
}

export default App;
