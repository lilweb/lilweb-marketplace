import React from "react";
import Board from "react-trello"; // eslint-disable-line import/no-unresolved

export default function Trello() {
  return (
    <Board
      style={{ backgroundColor: "transparent", justifyContent: "center" }}
      data={{
        lanes: [
          {
            id: "lane1",
            title: "Поступил запрос от клиента",
            label: "2/2",
            cards: [
              {
                id: "Card1", title: "Александр Пушкин", description: "Необходима интеграция CRM с облачным решением", label: "88005553535",
              },
              {
                id: "Card2", title: "Сергей Есенин", description: "Необходима интеграция CRM с облачным решением", label: "89876543210", metadata: { sha: "be312a1" },
              },
            ],
          },
          {
            id: "lane2",
            title: "В поиске партнера",
            label: "0/0",
            cards: [],
          },
          {
            id: "lane3",
            title: "Назначен исполнитель",
            label: "0/0",
            cards: [
              {
                id: "Card2", title: "Сергей Есенин", description: "Необходима интеграция CRM с облачным решением", label: "89876543210", metadata: { sha: "be312a1" },
              },
            ],
          },
          {
            id: "lane4",
            title: "Запрос выполнен исполнитель",
            label: "0/0",
            cards: [],
          },
        ],
      }}
    />
  );
}
