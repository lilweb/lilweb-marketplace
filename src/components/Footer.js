import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles({
  footer: {
    dispay: "flex",
    width: "100%",
    padding: 0,
    margin: 0,
    color: "white",
    minHeight: "auto",
    position: "relative",
    bottom: 0,
  },
  container: {
    paddingLeft: "40px",
    paddingRight: "40px",
    marginLeft: "auto",
    marginRight: "auto",
  },
  footerBottom: {
    width: "100%",
    borderTop: "1px solid rgba(151,151,151,0.11)",
    padding: "36px  40px",
    boxSizing: "border-box",
    marginTop: "45px",
  },
  footerText: {
    fontSize: 14,
    fontWeight: 400,
    lineHeight: 1.57,
    color: "#585858",
    textAlign: "left",
  },
  backdrop: {
    backdropFilter: "blur(20px)",
    padding: "89px 0 0",
  },
  footerMenuHeader: {
    fontSize: "20px",
    fontWeight: "500",
    lineHeight: "normal",
    color: "#fff",
    marginBottom: "25px",
    textAlign: "left",
  },
  background: {
    position: "absolute",
    background: "url(https://elevateyourexperiences.com/wp-content/uploads/2018/02/elevate-website-background.jpg)",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    zIndex: -2,
  },
  footerLink: {
    display: "inherit",
    textAlign: "left",
    fontSize: "14px",
    fontWeight: "400",
    fontStretch: "normal",
    fontStyle: "normal",
    lineHeight: "2.14",
    letterSpacing: "normal",
    color: "#919191",
    textDecoration: "none",
  },
  footerMenu: {
    width: "auto",
    // -webkitBoxOrient: "vertical",
    // -webkitBoxDirection: "normal",
    flexFlow: "column wrap",
    padding: 0,
    listStyle: "none",
    textAlign: "left",
  },
});
export default function Footer() {
  const classes = useStyles();
  return (
    <footer className={classes.footer}>
      <div className={classes.background} />
      <div className={classes.backdrop}>

        <div className={classes.container}>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <div className={classes.footerMenuHeader}>О нас</div>
              <p className={classes.footerText}>
                SberCloud — облачный провайдер нового поколения,
                предоставляющий услуги и решения на базе крупнейшей
                технологической ИТ-инфраструктуры
                и передовой экосистемы Группы Сбербанк, крупнейшей в России, СНГ и Восточной Европы.
              </p>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.footerMenuHeader}>Продукты</div>
              <ul className={classes.footerMenu}>
                <li>
                  <a href="/catalog/platform" className={classes.footerLink}>Облачная платформа</a>
                </li>
                <li>
                  <a href="/catalog/digital" className={classes.footerLink}>Цифровая платформа Сбербанка</a>
                </li>
                <li>
                  <a href="/catalog/aicloud" className={classes.footerLink}>AI Cloud</a>
                </li>
                <li>
                  <a href="/catalog/solutions" className={classes.footerLink}>Бизнес-решения</a>
                </li>
              </ul>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.footerMenuHeader}>Компания</div>
              <ul className={classes.footerMenu}>
                <li>
                  <a href="/about" className={classes.footerLink}>О компании</a>
                </li>
              </ul>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.footerMenuHeader}>Контакты</div>
              <div className="footer-contact-block">
                <div className={classes.footerText}>Техническая поддержка</div>
                <a href="tel:88004442499" className={classes.footerLink}>8 800 444-24-99</a>
                <a href="mailto:support@sbercloud.ru" className={classes.footerLink}>support@sbercloud.ru</a>
              </div>
              <div className="footer-contact-block">
                <div className={classes.footerText}>Офис</div>
                <a href="tel:+74952601082" className={classes.footerLink}>+7 495 260-10-82</a>
              </div>
              <div className="footer-contact-block">
                <div className={classes.footerText}>Отдел продаж</div>
                <a href="tel:+74952601081" className={classes.footerLink}>+7 495 260-10-81</a>
                <a href="mailto:info@sbercloud.ru" className={classes.footerLink}>info@sbercloud.ru</a>
              </div>

            </Grid>
          </Grid>
        </div>
        <div className={classes.footerBottom}>
          <Grid container>
            <Grid item xs={6}>
              <span className={classes.footerLink}>© 2019 SberCloud</span>
            </Grid>
            <Grid item xs={6}>
              <div>
                <a href="/agreement" className={classes.footerLink} style={{ textAlign: "right" }}>Политика обработки персональных данных</a>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    </footer>
  );
}
