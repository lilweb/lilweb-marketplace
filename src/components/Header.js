import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Logo } from ".";

const useStyles = (image) => makeStyles({
  container: {
    height: "100vh",
    backgroundSize: "cover",
  },
  mainHeader: {
    width: "100%",
    height: "100vh",
    display: "grid",
    gridTemplateColumns: "1fr",
    gridTemplateRows: "auto",
    zIndex: 2,
    background: "#eeeeee",
  },
  link: {
    textDecoration: "none",
    color: "inherit",
  },
  bottomContainer: {
    position: "absolute",
    bottom: 0,
    left: "0px",
    width: "100%",
  },
  waveBlock: {
    alignSelf: "end",
    display: "flex",
  },
  backgroundImageBlock: {
    filter: "brightness(0.5)",
    height: "100vh",
    width: "100%",
    position: "absolute",
    top: "0px",
    left: "0px",
    background: `url(${image}) center bottom / cover`,
    backgroundPosition: "center center",
  },
  navBar: {
    dispay: "flex",
  },
  eHSAxJ: {
    "&:hover": {
      "& $logoBeatles": {
        opacity: 1,
      },
    },
    position: "relative",
    display: "flex",
    minHeight: "auto",
    transform: "translate3d(0px, 0px, 0px)",
    cursor: "pointer",
    boxSizing: "border-box",
    overflow: "hidden",
    padding: "40px",
    background: "transparent",
    borderRadius: "14px",
    borderWidth: "initial",
    borderStyle: "none",
    borderColor: "initial",
    borderImage: "initial",
    height: "50vh",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  cardTitle: {
    color: "#ffffff",
    fontSize: "60px",
    margin: 0,
    flex: 1,
    textAlign: "left",
    height: "100%",
  },
  cardTitleText: {
    maxHeight: "100%",
    overflow: "hidden",
    color: "#ffffff",
    fontSize: "60px",
    margin: 0,
    textAlign: "left",
  },
  logoBeatles: {
    opacity: 0,
    position: "absolute",
    width: "100%",
    height: "100%",
    transition: "all 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    transform: "scale(1.6)",
    top: "-50%",
    left: "50%",
  },
  titleCard: {
    position: "absolute",
    top: "25vh",
    transform: "translate(50%)",
    height: "50vh",
    width: "50vw",
    boxShadow: "rgba(255, 255, 255, 0.5) 0px 10px 30px",
    transition: "box-shadow 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s, transform 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    overflow: "hidden",
    borderRadius: "18px",
    "&:hover": {
      boxShadow: "rgba(255, 255, 255, 0) 0px 10px 20px",


      transform: "translate(50%, -20px)",
    },
  },
  buttonGreen: {
    padding: "0.7em 1em",
    display: "flex",
    fontWeight: 600,
    fontSize: "20px",
    color: "white",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#03CE7E",
    transition: "box-shadow 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    borderRadius: "13px",
    "&:hover": {
      boxShadow: "rgba(3, 206, 126, 0.5) 0px 5px 15px",
    },
  },
})();
export default function Header(props) {
  const classes = useStyles(props.image);
  return (
    <div className={classes.mainHeader}>
      <div className={classes.backgroundImageBlock}>
        <div className={classes.navBar} />
      </div>
      <div className={classes.titleCard}>
        <div className={classes.eHSAxJ}>
          <div className={classes.cardTitle}>
            {props.main ? (<Logo className={classes.logoBeatles} />) : null }
            <h3 className={classes.cardTitleText}>{props.title}</h3>
          </div>

          <div className={classes.buttonGreen} role="presentation" onClick={props.onClick}>{props.buttonText}</div>
        </div>
      </div>
      <div className={classes.bottomContainer}>
        <div className={classes.waveBlock}>
          <svg width="100%" height="150px" fill="none">
            <path fill="#eeeeee" d="M 0 70.1657 C 237.866 49.0705 329.767 80.4501 626.357 25.8582 C 922.947 -28.734 1090.47 74.1178 1348.11 92.4591 C 1629.05 115.712 1893.91 76.4452 2560 70.1658 V 216.168 L 0 216.167 V 70.1657 Z"><animate repeatCount="indefinite" fill="freeze" attributeName="d" dur="10s" values="M0 25.9086C277 84.5821 433 65.736 720 25.9086C934.818 -3.9019 1214.06 -5.23669 1442 8.06597C2079 45.2421 2208 63.5007 2560 25.9088V171.91L0 171.91V25.9086Z; M0 86.3149C316 86.315 444 159.155 884 51.1554C1324 -56.8446 1320.29 34.1214 1538 70.4063C1814 116.407 2156 188.408 2560 86.315V232.317L0 232.316V86.3149Z; M0 53.6584C158 11.0001 213 0 363 0C513 0 855.555 115.001 1154 115.001C1440 115.001 1626 -38.0004 2560 53.6585V199.66L0 199.66V53.6584Z; M0 25.9086C277 84.5821 433 65.736 720 25.9086C934.818 -3.9019 1214.06 -5.23669 1442 8.06597C2079 45.2421 2208 63.5007 2560 25.9088V171.91L0 171.91V25.9086Z" /></path>
          </svg>
        </div>
      </div>
    </div>
  );
}
