import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Link,
  Redirect,
} from "react-router-dom";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Logo } from ".";


const useStyles = makeStyles({
  navBar: {
    position: "sticky",
    top: "0",
    maxWidth: "100vw",
    left: "320px",
    height: "40px",
    zIndex: 1000,
    background: "rgba(0,0,0,0.5)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: "1em 40px",
    backdropFilter: "blur(20px)",
  },
  navWrapper: {
    width: "100%",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  navLinkWrapper: {
    flex: 1,
    display: "flex",
    justifyContent: "flex-end",
  },
  navLinks: {
    width: "600px",
    height: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  link: {
    textDecoration: "none",
    height: "100%",
    color: "inherit",
  },
  navLogo: {
    height: "100%",
  },
  navLink: {
    "&:hover": {
      color: "black",
      background: "rgba(255,255,255,1)",
    },
    margin: "0 20px",
    background: "rgba(255,255,255,0)",
    transition: "all 0.8s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    fontWeight: 400,
    borderRadius: "0.6em",
    padding: "0.5em 0.7em",
    borderColor: "0.5em",
    color: "#ffffff",
    cursor: "pointer",
    fontSize: "20px",
    lineHeight: "30px",
  },

});


export default function NavBar() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [loggedIn, setLoggedIn] = React.useState(false);
  const [type, setType] = React.useState("");

  const checkAcc = () => {
    const accounts = ["admin", "user", "integrator", "saler", "salesman", "support"];
    if (accounts.includes(type)) {
      setLoggedIn(true);
    }
    setOpen(false);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      checkAcc();
    }
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className={classes.navBar}>
      <div className={classes.navWrapper}>
        <Link to="/" className={classes.link}>
          <Logo className={classes.navLogo} />
        </Link>
        <div className={classes.navLinkWrapper}>
          <div className={classes.navLinks}>
            <div className={classes.navLink}>
              <Link to="/catalog" className={classes.link}>Каталог решений</Link>
            </div>
            <div onClick={handleClickOpen} role="presentation" className={classes.navLink}>
              <div className={classes.link}>Личный кабинет</div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Личный кабинет</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Введите роль пользователя:
              <br />
              admin | user | integrator | saler | salesman | support
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Роль"
              placeholder="Роль"
              type="text"
              value={type}
              onChange={(e) => { setType(e.target.value); }}
              fullWidth
              onKeyPress={handleKeyPress}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Отмена
            </Button>
            <Button onClick={checkAcc} color="primary">
              Войти
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      {loggedIn ? <Redirect to={`/goToAccount/${type}`} /> : null}
    </div>
  );
}
