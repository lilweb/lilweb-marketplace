import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";
import clsx from "clsx";

const useStyles = makeStyles({
  productCard: {
    "&$productCardWithIcons": {
      "&:hover": {
        "&::before": {
          opacity: 0.8,
        },
      },
      "& $productCardContent": {
        zIndex: 2,
      },
      "&::before": {
        content: "''",
        background: "rgba(0,0,0,1)",
        position: "absolute",
        width: "100%",
        height: "100%",
        left: 0,
        top: 0,
        zIndex: 1,
        opacity: 0,
        transition: "opacity 0.6s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
      },
    },
    "&:hover": {
      "&::before": {
        opacity: 0.8,
      },
      boxShadow: "rgba(0, 0, 0, 0.35) 0px 30px 60px",
      transform: "scale(1.1, 1.1)",
      "& $productCardImage": {
        transform: "translateY(20px)",
      },
    },
    width: "300px",
    height: "225px",
    boxSizing: "border-box",
    position: "relative",
    display: "grid",
    gridTemplateRows: "1fr 1fr",
    boxShadow: "rgba(0, 0, 0, 0.15) 0px 20px 40px",
    cursor: "pointer",
    zIndex: "1",
    justifySelf: "center",
    backfaceVisibility: "hidden",
    transform: "translate3d(0px, 0px, 0px)",
    borderRadius: "20px",
    padding: "20px",
    overflow: "hidden",
    transition: "all 0.6s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
  },
  productCardWithIcons: {},
  productCardImage: {
    transition: "all 1s cubic-bezier(0.2, 0.8, 0.2, 1) 0s",
    position: "absolute",
    top: "-20px",
    left: "-20px",
    width: "360px",
    height: "265px",
    background: "url(//images.ctfassets.net/ooa29xqb8tix/7j3mlXoU1N8XmEGhEJitZ2/e8658263ccde923351d2afffdfd62a2d/react-chapter.jpg?w=720&h=400&q=95&fit=fill) center center / cover",
    backgroundPosition: "center center",
  },
  productCardOverlay: {
    position: "absolute",
    top: "0px",
    width: "100%",
    height: "225px",
    background: "linear-gradient(-180deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.5) 100%)",
  },
  productCardTitle: {
    lineHeight: "120%",
    textShadow: "rgba(0, 0, 0, 0.15) 0px 10px 20px",
    fontSize: "30px",
    fontWeight: "700",
    textAlign: "left",
    color: "white",
    margin: "0px",
    background: "linear-gradient(104.74deg, white 0%, white 100%) text",
  },
  productCardContent: {
    position: "relative",
    display: "flex",
  },
});
function ProductCard(props) {
  const classes = useStyles();
  const cardClass = props.cardIcons
    ? clsx(classes.productCard, classes.productCardWithIcons)
    : classes.productCard;
  return (
    <div className={cardClass} role="presentation" onClick={() => props.history.push("/product")}>
      <div className={classes.productCardImage} />
      <div className={classes.productCardOverlay} />
      <div className={classes.productCardContent}>
        <h3 className={classes.productCardTitle}>IT Решение</h3>
      </div>
    </div>
  );
}
export default withRouter(ProductCard);
