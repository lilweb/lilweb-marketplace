import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import { withRouter } from "react-router-dom";
import Add from "@material-ui/icons/Add";
import { ProductCard } from ".";

const useStyles = makeStyles({
  container: {
    minHeight: "100vh",
    boxSizing: "border-box",
  },
  titleWrapper: {
    textAlign: "center",
    margin: "50px 0",
    padding: "0px 20px",
  },
  title: {
    fontSize: "50px",
    fontWeight: 500,
  },
  productWrapper: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr))",
    margin: "30px auto",
    gap: "30px",
    maxWidth: "960px",
    padding: "0px 20px",
  },
  addButton: {
    fontSize: "70px",
  },
  addBlock: {
    height: "10em",
    width: "10em",
    borderRadius: "13px",
    dispay: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
  },
  flexer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});
const defaultProducts = Array(9).fill(0);
function Products(props) {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <div className={classes.titleWrapper}>
        <h3 className={classes.title}>{props.title}</h3>
      </div>
      {
          props.salerAcc ? (
            <div className={classes.flexer}>
              <div className={classes.addBlock} role="presentation" onClick={() => props.history.push("/account/product-edit")}>
                <Tooltip title="Добавить продукт" placement="top">
                  <Add className={classes.addButton} />
                </Tooltip>
              </div>
            </div>
          ) : null
      }

      <div className={classes.productWrapper}>
        {defaultProducts.map(() => (
          <ProductCard cardIcons={props.cardIcons} />
        ))}
      </div>
    </div>
  );
}
export default withRouter(Products);
