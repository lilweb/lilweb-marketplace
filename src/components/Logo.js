import React from "react";

export default function Logo(props) {
  return (
    <svg className={props.className} xmlns="http://www.w3.org/2000/svg" width="80" height="100%" viewBox="0 0 80 96" fill="none">
      <g mask="url(#mask0)">
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 25.8246H44.216V18.1046H36.496V25.8246Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 44.0296H44.216V36.3096H36.496V44.0296Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 51.6576H44.216V43.9376H36.496V51.6576Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 33.5236H44.216V25.8036H36.496V33.5236Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 7.7196H44.216V-0.000396729H36.496V7.7196Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M18.248 20.6006H25.968V12.8806H18.248V20.6006Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M18.248 38.6336H25.968V30.9136H18.248V38.6336Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M18.248 46.3626H25.968V38.6426H18.248V46.3626Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M0 33.5236H7.72V25.8036H0V33.5236Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M0 51.6576H7.72V43.9376H0V51.6576Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M54.743 20.6006H62.463V12.8806H54.743V20.6006Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M54.743 38.6336H62.463V30.9136H54.743V38.6336Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M54.743 46.3626H62.463V38.6426H54.743V46.3626Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M72.991 33.5236H80.711V25.8036H72.991V33.5236Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 77.4916H44.216V69.7706H36.496V77.4916Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 59.2856H44.216V51.5646H36.496V59.2856Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 69.7916H44.216V62.0706H36.496V69.7916Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M36.496 95.5956H44.216V87.8756H36.496V95.5956Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M54.743 82.7146H62.463V74.9946H54.743V82.7146Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M54.743 64.6816H62.463V56.9616H54.743V64.6816Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M54.743 56.9526H62.463V49.2326H54.743V56.9526Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M72.991 69.7916H80.711V62.0706H72.991V69.7916Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M18.248 82.7146H25.968V74.9946H18.248V82.7146Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M18.248 64.6816H25.968V56.9616H18.248V64.6816Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M18.248 56.9526H25.968V49.2326H18.248V56.9526Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M0 69.7916H7.72V62.0706H0V69.7916Z" fill="#03CE7E" />
        <path fillRule="evenodd" clipRule="evenodd" d="M72.991 51.6576H80.711V43.9376H72.991V51.6576Z" fill="#03CE7E" />
      </g>
    </svg>
  );
}
