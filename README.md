# Lilweb Marketplace
Описание стэка технологий клиентской части:
Backend (NodeJs)

"babel-preset-env": "^1.7.0",
"babel-register": "^6.26.0",
"bcryptjs": "^2.4.3",
"express": "^4.17.1",
"helmet": "^3.21.2",
"mongoose": "^5.7.13"

Frontend (NodeJS) ReactJS, Material-UI
{
  "name": "lilweb-marketplace",
  "version": "0.1.0",
  "private": true,
  "dependencies": {
    "@material-ui/core": "^4.7.0",
    "@material-ui/icons": "^4.5.1",
    "babel-eslint": "^10.0.3",
    "chartist": "0.10.1",
    "classnames": "2.2.6",
    "clsx": "^1.0.4",
    "history": "4.9.0",
    "perfect-scrollbar": "1.4.0",
    "prop-types": "^15.7.2",
    "react": "^16.12.0",
    "react-chartist": "0.13.3",
    "react-dom": "^16.12.0",
    "react-google-maps": "9.4.5",
    "react-markdown": "^4.2.2",
    "react-router-dom": "^5.1.2",
    "react-scripts": "3.2.0",
    "react-swipeable-views": "0.13.3"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eslint": "eslint --ext .js src --fix",
    "eject": "react-scripts eject"
  },
  "eslintConfig": {
    "extends": "react-app"
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  },
  "devDependencies": {
    "eslint": "^6.7.1",
    "eslint-config-airbnb": "^18.0.1",
    "eslint-plugin-import": "^2.18.2",
    "eslint-plugin-jsx-a11y": "^6.2.3",
    "eslint-plugin-react": "^7.17.0",
    "eslint-plugin-react-hooks": "^1.7.0",
    "raw-loader": "^4.0.0",
    "react-app-rewired": "^2.1.5"
  }
}


Для запуска проекта необходимо произвести следующие действия:
Загрузить файлы из репозитория. В корневой папке запустить из консоли команду "npm i". Далее "npm start". Обязательно наличие NodeJS


Для запуска бэкэнда повтрорить действия в папке ./lilmarket-backend находящейся в корне репозитория.
IT solutions marketplace created by LilWeb team
